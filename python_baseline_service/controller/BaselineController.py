from ..factory import DatasetFactory
import json
from ..services.HTTPClient import HTTPClient
from ..services.BaselineService import BaselineService
from ..services.FileManager import FileManager
from ..services import ExcelIO
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
import time
from django.core.files import File
import codecs


# ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
@csrf_exempt
def get_requested_baselines(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        
        path = data['Path']
        file = FileManager.get_file(path)
        dataframe = ExcelIO.read_excel_to_dataframe(file)
        dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)

        ranges_of_variation = data['Ranges of Variation']
        target_column = data['Target Column']
        target_days_of_week = data['Target Days of Week']

        methods_list = data['Methods']

        results = BaselineService.get_baselines(dataset, target_column, target_days_of_week, ranges_of_variation, methods_list)
        return JsonResponse(results)
    else:
        return HttpResponseNotFound('Wrong request method')


@csrf_exempt
def get_available_methods(request):
    if request.method == 'GET':
        available_methods = HTTPClient.get_available_methods()
        return JsonResponse(available_methods)
    else:
        return HttpResponseNotFound('Wrong request method')


@csrf_exempt
def download_results(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        path = data['path']
        dataframe = ExcelIO.read_excel_to_dataframe(path).to_numpy().tolist()
        return JsonResponse({'data': dataframe})
        #
        #path_to_file = os.path.realpath(path)
        f = codecs.open(path, 'r', encoding="utf8", errors='ignore')
        myfile = File(f)
        response = HttpResponse(myfile, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=' + 'Results.xslx'
        return response
    else:
        return HttpResponseNotFound('Wrong request method')


############################# ! OLD FLUX ##############################################################################

# ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
@csrf_exempt
def save_all_baselines_from_file(request):
    if request.method == 'POST':
        target_column = request.POST.get('targetColumn')
        file = request.FILES['file']
        path = FileManager.upload_file(file)
        dataframe = ExcelIO.read_excel_to_dataframe(path)
        dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
        response = BaselineService.get_all_baselines(dataset, target_column)
        # todo return json response with object with mesasge and also the data for the graphs
        FileManager.delete_file(path)
        print("ALL BASELINES DONE")
        return JsonResponse(response)
    else:
        return HttpResponseNotFound('Wrong request method')


@csrf_exempt
def save_all_baselines_from_file_weekdays(request):
    if request.method == 'POST':
        target_column = request.POST.get('targetColumn')
        file = request.FILES['file']
        path = FileManager.upload_file(file)
        dataframe = ExcelIO.read_excel_to_dataframe(path)
        dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
        response = BaselineService.get_all_baselines_weekdays(
            dataset, target_column)
        FileManager.delete_file(path)
        print("WEEKDAYS BASELINES DONE")
        return JsonResponse(response)
    else:
        return HttpResponseNotFound('Wrong request method')


@csrf_exempt
def save_all_baselines_from_file_weekends(request):
    if request.method == 'POST':
        target_column = request.POST.get('targetColumn')
        file = request.FILES['file']
        path = FileManager.upload_file(file)
        dataframe = ExcelIO.read_excel_to_dataframe(path)
        dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
        response = BaselineService.get_all_baselines_weekends(
            dataset, target_column)
        FileManager.delete_file(path)
        print("WEEKENDS BASELINES DONE")
        return JsonResponse(response)
    else:
        return HttpResponseNotFound('Wrong request method')
