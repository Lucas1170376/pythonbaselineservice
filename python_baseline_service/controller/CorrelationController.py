from django.views.decorators.csrf import csrf_exempt
from ..services.FileManager import FileManager
from ..services import ExcelIO
from ..factory import DatasetFactory
import json
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponse, HttpResponseNotFound
from python_baseline_service.services.CorrelationService import CorrelationService

class CorrelationController:

    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def get_correlations(request):
        if request.method == 'POST':
            data = json.loads(request.body.decode('utf-8'))
            path = data['path']
            file = FileManager.get_file(path)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            correlations = CorrelationService.get_all_correlations(dataset)
            return JsonResponse({ 'correlations': correlations })
        else:
            return HttpResponseNotFound('Wrong request method')


    #####################? OLD STUFF ####################################

    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def get_all_correlations_from_excel(request):
        if request.method == 'POST':
            target_column = request.POST.get('targetColumn')
            file = request.FILES['file']
            path = FileManager.upload_file(file)
            dataframe = ExcelIO.read_excel_to_dataframe(path)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            #call service for correlations
            correlations = get_all_correlations(dataset, target_column)
            FileManager.delete_file(path)
            return JsonResponse(correlations)
        else:
            return HttpResponseNotFound('Wrong request method')

    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def get_correlations_without_previous_target_values(request):
        if request.method == 'POST':
            filepath = request.FILES['file']
            file = FileManager.get_file(filepath)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            