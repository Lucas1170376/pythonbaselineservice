from ..factory import DatasetFactory
import json
from ..services.HTTPClient import HTTPClient
from ..services.FileUploadService import FileUploadService
from ..services.FileManager import FileManager
from ..services import ExcelIO
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt

class FileUploadController:

    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def upload_file(request):
        if request.method == 'POST':
            file = request.FILES['file']
            #? this allows only for one uploaded file at the same time, which implies only one user can be using the system
            #? in the future delete old files in other way, for now is commented tho
            # todo find way to automatically delete files
            #FileManager.clean_folder()
            filename = FileManager.save_file(file.name, file)
            file = FileManager.get_file(filename)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            column_list = FileUploadService.get_columns_from_dataset(dataset)
            info = FileUploadService.get_info_from_dataset(dataset)
            available_methods = HTTPClient.get_available_methods()
            return JsonResponse({ 'pathToFile': filename, 'columnList': column_list, 'info': info, 'availableMethods': available_methods})
        else:
            return HttpResponseNotFound('Wrong request method')