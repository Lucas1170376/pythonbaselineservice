import pandas
from pandas import isnull
from openpyxl import Workbook
from openpyxl import load_workbook
import xlsxwriter
from python_baseline_service.services.SettingsManager import SettingsManager


YEAR_COLUMN_TITLE = SettingsManager.get_year_column_title()
MONTH_COLUMN_TITLE = SettingsManager.get_month_column_title()
DAY_OF_WEEK_COLUMN_TITLE = SettingsManager.get_day_of_week_column_title()
DAY_COLUMN_TITLE = SettingsManager.get_day_column_title()
TIME_PERIOD_COLUMN_TITLE = SettingsManager.get_time_period_column_title()


def write_baselines_to_excel(file_path, data):

    write_excel_columns(file_path)

    wb = load_workbook(file_path)
    ws = wb.active
    current_row = ws.max_row + 1
    
    for result in data:
        baselineResults = result['Results']
        time_period = result[TIME_PERIOD_COLUMN_TITLE]
        day = result[DAY_COLUMN_TITLE]
        month = result[MONTH_COLUMN_TITLE]
        year = result[YEAR_COLUMN_TITLE]
        day_of_week = result[DAY_OF_WEEK_COLUMN_TITLE]
        real_value = result['Real Value']
        for baselineResult in baselineResults[0]:
            previousDays = baselineResult[0]
            method = baselineResult[1]
            baselineValue = baselineResult[2]
            ws.cell(row = current_row, column = 1, value = year)
            ws.cell(row = current_row, column = 2, value = month)
            ws.cell(row = current_row, column = 3, value = day_of_week)
            ws.cell(row = current_row, column = 4, value = day)
            ws.cell(row = current_row, column = 5, value = time_period)
            ws.cell(row = current_row, column = 6, value = previousDays)
            ws.cell(row = current_row, column = 7, value = method)
            ws.cell(row = current_row, column = 8, value = baselineValue)
            ws.cell(row = current_row, column = 9, value = real_value)
            current_row += 1
    wb.save(file_path)

def write_excel_columns(file_path):
    wb = Workbook()
    ws = wb.active
    ws.cell(row = 1, column = 1, value = 'Year')
    ws.cell(row = 1, column = 2, value = 'Month')
    ws.cell(row = 1, column = 3, value = 'Day of Week')
    ws.cell(row = 1, column = 4, value = 'Day')
    ws.cell(row = 1, column = 5, value = 'Time Period')
    ws.cell(row = 1, column = 6, value = 'Previous Days Used')
    ws.cell(row = 1, column = 7, value = 'Method')
    ws.cell(row = 1, column = 8, value = 'Baseline')
    ws.cell(row = 1, column = 9, value = 'Real Value')
    wb.save(file_path)

#############################! old flux ###############################################

#this is specifically for the errors matrix in the get all baselines request
def write_error_matrix_to_excel(matrix, excel_file_name):
    workbook = xlsxwriter.Workbook(excel_file_name)
    worksheet = workbook.add_worksheet()
    row = 0
    col = 0
    worksheet.write(row, col, "Previous days and method")
    worksheet.write(row, col + 1, "Mean of errors")
    worksheet.write(row, col + 2, "Minimum error")
    worksheet.write(row, col + 3, "Maximum error")
    for i in range(len(matrix) - 1):
        worksheet.write(i + 1, col, "Previous Days:" + str(matrix[i][0]) + "; Method:" + str(matrix[i][1]))
        worksheet.write(i + 1, col + 1, matrix[i][2])
        worksheet.write(i + 1, col + 2, matrix[i][3])
        worksheet.write(i + 1, col + 3, matrix[i][4])
    workbook.close()


def write_matrix_to_excel(matrix, excel_file_name):
    workbook = xlsxwriter.Workbook(excel_file_name)
    worksheet = workbook.add_worksheet()
    row = 0
    for col, data in enumerate(matrix):
        worksheet.write_column(row, col, data)
    workbook.close()


def read_excel_to_dataframe(file_path):
    df = pandas.read_excel(file_path)
    first_row = df.iloc[0]
    first_row_list = list(first_row)
    #this deletes excess columns that pandas might read
    while isnull(first_row_list[-1]):
        df.drop(df.columns[len(df.columns) - 1], axis=1, inplace=True)
        first_row = df.iloc[0]
        first_row_list = list(first_row)
    return df


def write_baselines_to_excel1(data, time_period, day, month, year, day_of_week, workbook, worksheet):
    row = 1
    col = 0
    baselines = data.baselines
    for baseline in baselines:
        baselineValue = baseline.baseline
        method = baseline.method
        previousDays = baseline.previousDays
        worksheet.write(row, col, time_period)
        worksheet.write(row, col + 1, day)
        worksheet.write(row, col + 2, month)
        worksheet.write(row, col + 3, year)
        worksheet.write(row, col + 4, day_of_week)
        worksheet.write(row, col + 5, previousDays)
        worksheet.write(row, col + 6, method)
        worksheet.write(row, col + 7, baselineValue)
        row += 1
    workbook.close()


def write_excel_header(file_path):
    wb = Workbook()
    ws = wb.active
    ws.cell(row = 1, column = 1, value = 'Time Period')
    ws.cell(row = 1, column = 2, value = 'Day')
    ws.cell(row = 1, column = 3, value = 'Month')
    ws.cell(row = 1, column = 4, value = 'Year')
    ws.cell(row = 1, column = 5, value = 'Day of Week')
    ws.cell(row = 1, column = 6, value = 'Previous Days Used')
    ws.cell(row = 1, column = 7, value = 'Method')
    ws.cell(row = 1, column = 8, value = 'Baseline')
    wb.save(file_path)

def write_baselines_to_excel3(file_path, data, time_period, day, month, year, day_of_week):
    wb = load_workbook(file_path)
    ws = wb.active
    current_row = ws.max_row + 1
    
    baselineResults = data.baselines
    for baselineResult in baselineResults:
        baselineValue = baselineResult.baseline
        method = baselineResult.method
        previousDays = baselineResult.previousDays
        ws.cell(row = current_row, column = 1, value = time_period)
        ws.cell(row = current_row, column = 2, value = day)
        ws.cell(row = current_row, column = 3, value = month)
        ws.cell(row = current_row, column = 4, value = year)
        ws.cell(row = current_row, column = 5, value = day_of_week)
        ws.cell(row = current_row, column = 6, value = previousDays)
        ws.cell(row = current_row, column = 7, value = method)
        ws.cell(row = current_row, column = 8, value = baselineValue)
        current_row += 1
    
    wb.save(file_path)


def write_baselines_to_excel2(file_path, data):
    wb = load_workbook(file_path)
    ws = wb.active
    current_row = ws.max_row + 1
    
    for result in data:
        baselineResults = result['results'].baselines
        time_period = result['time_period']
        day = result['day']
        month = result['month']
        year = result['year']
        day_of_week = result['day_of_week']
        for baselineResult in baselineResults:
            baselineValue = baselineResult.baseline
            method = baselineResult.method
            previousDays = baselineResult.previousDays
            ws.cell(row = current_row, column = 1, value = time_period)
            ws.cell(row = current_row, column = 2, value = day)
            ws.cell(row = current_row, column = 3, value = month)
            ws.cell(row = current_row, column = 4, value = year)
            ws.cell(row = current_row, column = 5, value = day_of_week)
            ws.cell(row = current_row, column = 6, value = previousDays)
            ws.cell(row = current_row, column = 7, value = method)
            ws.cell(row = current_row, column = 8, value = baselineValue)
            current_row += 1
    
    wb.save(file_path)
