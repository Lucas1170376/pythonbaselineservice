from ..model.Dataset import Dataset
from django.core.files.storage import default_storage
from ..services.ExcelIO import read_excel_to_dataframe
from pathlib import Path

class FileManager:

    def save_file(filename, file):
        timed_filename = FileManager.get_timed_filename(filename) 
        file_name = default_storage.save(timed_filename, file)
        return file_name

    def get_file(filename):
        file = default_storage.open(filename)
        file_url = default_storage.url(filename)
        return file

    def delete_file(filename):
        default_storage.open(filename).read()
        default_storage.delete(filename)

    def clean_folder():
        file_list = default_storage.listdir(settings.MEDIA_ROOT)[1]
        for file in file_list:
            FileManager.delete_file(file)

    def get_timed_filename(filename):
        from datetime import datetime
        now = datetime.now()
        current_time = str(now.strftime("%d_%m_%Y__%H_%M_%S"))
        split_filename = filename.split('.')
        extension = split_filename[-1]
        timed_filename = ""
        for part in split_filename:
            if part != extension:
                timed_filename += part
        return timed_filename + "_" + current_time + "." + extension

    def save_results_file(file):
        timed_filename = FileManager.get_timed_results_filename()
        file_name = default_storage.save(timed_filename, file)
        return file_name

    # todo might not be multiple request ready
    def get_timed_results_filename():
        from datetime import datetime
        now = datetime.now()
        current_time = str(now.strftime("%d_%m_%Y__%H_%M_%S"))
        return 'Res_' + current_time + ".xlsx"


###############?? Delete this ??########################
def get_available_results_path():
    from datetime import datetime
    # todo check if the filename exists, if so, get new datetime now until it is available
    now = datetime.now()
    current_time = str(now.strftime("%d_%m_%Y__%H_%M_%S"))
    return "results/Results_" + current_time + ".xlsx"


def upload_file(file):
    #todo this not workin
    new_file = Path('files/' + str(file))
    if new_file.is_file():
        i = 1
        path = 'files/' + str(i) + str(file)
        new_file = Path(path)
        while new_file.is_file():
            i += 1
            path = 'files/' + str(i) + str(file)
            new_file = Path(path)
        return default_storage.save(path, file)
    else:
        return default_storage.save('files/' + str(file), file)



def delete_file(file):
    default_storage.delete(file)
    # todo this not working, using the above for now
    '''
    if not default_storage.exists('files/' + str(file)):
        return False
    else:
        default_storage.delete(file)
        return True
    '''


def replace_file(file):
    if not default_storage.exists('files/' + str(file)):
        path = default_storage.save('files/' + str(file), file)
    else:
        default_storage.delete(file)
        path = default_storage.save('files/' + str(file), file)
    return path


def get_dataset_info_for_result_presentation(dataset, initial_index):
    first_day_object = dataset.get_given_index_date_object(initial_index)
    last_day_object = dataset.get_last_day_date_object()
    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    return {'first_day': first_day_object, 'last_day': last_day_object, 'number_of_periods_per_day': 
            number_of_periods_per_day}

