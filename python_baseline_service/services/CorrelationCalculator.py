import pandas as pd
from ..model.Dataset import Dataset
from .SettingsManager import SettingsManager

def get_dataframe_of_target_value_and_previous_target_values(dataset: Dataset, target_column):
    max_previous_days = SettingsManager.get_max_previous_days_for_baseline_computation()
    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    current_index = max_previous_days * number_of_periods_per_day
    initial_index = current_index

    list_of_lists_of_previous_values = []
    #append X extra columns to save a list of previous values for X days, X-1 days etc.. these lists will be used for correlation
    for i in range(max_previous_days):
        list_of_lists_of_previous_values.append([])

    for _ in range(dataset.get_number_of_rows() - initial_index):
        current_row = dataset.get_row_from_index(current_index)
        # obtaining needed info from that file
        time_period = current_row['Time Period']
        day = current_row['Day']
        month = current_row['Month']
        year = current_row['Year']
        day_of_week = current_row['Day of Week']

        values = dataset.get_target_values_from_X_previous_days(current_index, target_column, max_previous_days)
        for i in range(max_previous_days):
            list_of_lists_of_previous_values[i].append(values[i])

        current_index += 1

    valuesDict = {}
    for i in range(max_previous_days):
        string = str(target_column) + ' ' + str(max_previous_days - i) + " days before"
        valuesDict[string] = list_of_lists_of_previous_values[i]

    valuesDataframe = pd.DataFrame(valuesDict)
    return valuesDataframe


def get_correlation_between_target_value_and_previous_target_values(dataset: Dataset, target_column):
    max_previous_days = get_max_previous_days_for_baseline_computation()
    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    current_index = max_previous_days * number_of_periods_per_day
    initial_index = current_index

    list_of_lists_of_previous_values = []
    #append and extra list for the real target value
    list_of_lists_of_previous_values.append([])
    #append X extra columns to save a list of previous values for X days, X-1 days etc.. these lists will be used for correlation
    for i in range(max_previous_days):
        list_of_lists_of_previous_values.append([])

    for _ in range(dataset.get_number_of_rows() - initial_index):
        current_row = dataset.get_row_from_index(current_index)
        # obtaining needed info from that file
        time_period = current_row['Time Period']
        day = current_row['Day']
        month = current_row['Month']
        year = current_row['Year']
        day_of_week = current_row['Day of Week']
        real_target_value = dataset.get_real_target_value(time_period, day, month, year, target_column)
        list_of_lists_of_previous_values[0].append(real_target_value)

        values = dataset.get_target_values_from_X_previous_days(current_index, target_column, max_previous_days)
        for i in range(max_previous_days):
            list_of_lists_of_previous_values[i + 1].append(values[i])

        current_index += 1

    valuesDict = {}
    valuesDict['Target Value'] = list_of_lists_of_previous_values[0]
    for i in range(max_previous_days):
        string = 'Target Value ' + str(max_previous_days - i) + " days before"
        valuesDict[string] = list_of_lists_of_previous_values[i + 1]

    valuesDataframe = pd.DataFrame(valuesDict)

    results = valuesDataframe.corr(method = 'pearson')
    return results