from .SettingsManager import *
from .HTTPClient import request_all_baselines
from ..model.Dataset import Dataset
from .ExcelIO import write_baselines_to_excel
from .ExcelIO import write_baselines_to_excel2
from .ExcelIO import write_excel_header
from .ExcelIO import write_error_matrix_to_excel
from ..services import ExcelIO
from .FileManager import get_available_results_path
from .FileManager import get_dataset_info_for_result_presentation
from ..factory import DatasetFactory

import pandas as pd

YEAR_COLUMN = get_year_column_title()
MONTH_COLUMN = get_month_column_title()
DAY_OF_WEEK_COLUMN = get_day_of_week_column_title()
DAY_COLUMN = get_day_column_title()
PERIOD_COLUMN = get_time_period_column_title()
IS_FILTERED_COLUMN_TITLE = get_is_filtered_column_title()
IS_FILTERED = get_is_filtered()
IS_NOT_FILTERED = get_is_not_filtered()
PREVIOUS_INDEXES_FOR_ADJUSTMENT = get_previous_indexes_for_adjustment()

def get_requested_baselines(data):
    target_column = data['Target Column']
    path = data['Path']
    dataframe = ExcelIO.read_excel_to_dataframe("files/" + path)
    dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
    if not dataset.validate_target_column(target_column):
        return {'message': 'The chosen target column is not in the file'}
    
    target_days_of_week = data['Target Days of Week']
    selected_values = data['Selected Values']
    
    # filters the dataset by the days of week specified by client
    new_data = dataset.data[(dataset.data[DAY_OF_WEEK_COLUMN].isin(target_days_of_week))]

    # applies other filters provided by client
    for column in selected_values:
        new_data = new_data[(new_data[column] >= selected_values[column]['min']) & (new_data[column] <= selected_values[column]['max'])]

    filtered_dataset = Dataset(new_data)

    number_of_rows = dataset.get_number_of_rows()
    new_number_of_rows = filtered_dataset.get_number_of_rows()

    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    
    if new_number_of_rows <= get_max_previous_days_for_baseline_computation() * number_of_periods_per_day:
        return {'message': 'There is not sufficient historical data to compute baselines'}

    dataset_with_mark = add_filter_mark(dataset, filtered_dataset)
    results_list = compute_baselines(dataset_with_mark, target_column)
    error_info = get_error_information(results_list)
    time_info = get_time_information(results_list)
    path = get_available_results_path()
    ExcelIO.write_baselines_to_excel(path, results_list)
    return {'Results': results_list, 'Errors': error_info, 'TimeInfo': time_info, 'Path': path}


def add_filter_mark(original_dataset, filtered_dataset):
    filter_mark_column = []
    for index in range(original_dataset.get_number_of_rows()):
        row = original_dataset.get_row_from_index_df(index)
        year = int(row[YEAR_COLUMN])
        month = int(row[MONTH_COLUMN])
        day = int(row[DAY_COLUMN])
        time_period = int(row[PERIOD_COLUMN])
        possible_time_period_rows = filtered_dataset.data.loc[filtered_dataset.data[PERIOD_COLUMN] == time_period]
        possible_day_rows = possible_time_period_rows.loc[possible_time_period_rows[DAY_COLUMN] == day]
        possible_month_row = possible_day_rows.loc[possible_day_rows[MONTH_COLUMN] == month]
        target_row_dataframe = possible_month_row.loc[possible_month_row[YEAR_COLUMN] == year]
        if len(target_row_dataframe) >= 1:
            filter_mark_column.append(IS_FILTERED)
        else:
            filter_mark_column.append(IS_NOT_FILTERED)

    original_dataset.data[IS_FILTERED_COLUMN_TITLE] = filter_mark_column
    return original_dataset


def compute_baselines(dataset, target_column):
    max_previous_days = get_max_previous_days_for_baseline_computation()
    number_of_periods_per_day = dataset.get_number_of_periods_per_day()    
    # minimum historical data is the max_previous_days value (default = 10)
    # so we 'walk' those values forward
    initial_index = max_previous_days * number_of_periods_per_day
    current_index = initial_index
    results_list = []    
    for _ in range(dataset.get_number_of_rows() - initial_index):
        # gets the whole row from the dataset with that index
        row = dataset.get_row_from_index(current_index)
        if not row[IS_FILTERED_COLUMN_TITLE] == IS_FILTERED:
            #skip this iteration of the loop if the row is not on the filter
            current_index += 1
            continue
        # obtaining needed info from that row
        year = int(row[YEAR_COLUMN])
        month = int(row[MONTH_COLUMN])
        day_of_week = int(row[DAY_OF_WEEK_COLUMN])
        day = int(row[DAY_COLUMN])
        time_period = int(row[PERIOD_COLUMN])
        #obtaining the real target value to compute the error later
        real_target_value = float(dataset.get_real_target_value(time_period, day, month, year, target_column))
        #prepares the request body for the baseline calculation service, if there was no data to build it, skip this iteration of the loop
        baseline_request_body = get_baseline_calculation_service_request_body(dataset, current_index, target_column, time_period, day, month, year)
        if baseline_request_body == False:
            current_index += 1
            continue
        results = request_all_baselines(baseline_request_body)
        if hasattr(results, 'error'):
            return {'message': "error from baseline computation server " + str(results.error)}
        # if all went well, append results and their time information to the results list
        results_list.append({YEAR_COLUMN: year, MONTH_COLUMN: month, DAY_OF_WEEK_COLUMN: day_of_week, DAY_COLUMN: day, 
                                    PERIOD_COLUMN: time_period, 'Results': results, 'Real Value': real_target_value })
        current_index += 1
    return results_list


def get_baseline_calculation_service_request_body(dataset, current_index, target_column, time_period, day, month, year):
    # this gets the object with the adjustment values to be used for baseline computation
    adjustment_values = dataset.get_adjustment_values(current_index, target_column)
    #if adjustment values could not be found, or did not fit in the filter, the computation is not possible
    if adjustment_values == False:
        return False

    # this gets the matrix of target values from the given amount of previous_days in the first parameter
    target_matrix = dataset.get_matrix_for_previous_days(time_period, day, month, year, target_column)
    # if the previous method returned false, it means there was not enough data found to fill the matrix, an error is retured to the client
    if target_matrix == False:
        return False

    # building the object that will be transformed to json in order to make the request to the baseline calculation server
    baseline_request_body = {}
    baseline_request_body['consumptionDataset'] = target_matrix
    baseline_request_body['adjustmentValues'] = adjustment_values
    baseline_request_body['targetPeriodIndex'] = len(PREVIOUS_INDEXES_FOR_ADJUSTMENT)
    return baseline_request_body


def get_error_information(results_list):
    errors_map = {}
    for result in results_list:
        results = result['Results']
        real_value = result['Real Value']
        baselines = results[0]
        for baseline in baselines:
            previous_days_used = baseline[0]
            method = baseline[1]
            baseline_value = round(float(baseline[2]),3)
            # gets the error rounded to 3 decimal cases
            error = round(float(abs(real_value - baseline_value)), 3)
            #appends the information to the map, in order to return it to the client
            errors_map = append_error_information(errors_map, previous_days_used, method, error)
    return errors_map

def append_error_information(errors_map, previous_days, method, error):
    map_key = str(previous_days)+";"+str(method)
    if map_key not in errors_map:
        errors_map[map_key] = {'minError': 999999.000, 'maxError': 0.000, 'errorSum': 0.000, 'errorCount': 0}
    if error <= errors_map[map_key]['minError']:
        errors_map[map_key]['minError'] = error
    if error >= errors_map[map_key]['maxError']:
        errors_map[map_key]['maxError'] = error
    errors_map[map_key]['errorSum'] += error
    errors_map[map_key]['errorCount'] += 1
    return errors_map

def get_time_information(results_list):
    time_map = {YEAR_COLUMN: {'min': 999999, 'max': 0},MONTH_COLUMN: {'min': 999999, 'max': 0},DAY_OF_WEEK_COLUMN: {'min': 999999, 'max': 0},
    DAY_COLUMN: {'min': 999999, 'max': 0},PERIOD_COLUMN: {'min': 999999, 'max': 0}}
    for result in results_list:
        year = result[YEAR_COLUMN]
        if year >= time_map[YEAR_COLUMN]['max']:
            time_map[YEAR_COLUMN]['max'] = year
        if year <= time_map[YEAR_COLUMN]['min']:
            time_map[YEAR_COLUMN]['min'] = year

        month = result[MONTH_COLUMN]
        if month >= time_map[MONTH_COLUMN]['max']:
            time_map[MONTH_COLUMN]['max'] = month
        if month <= time_map[MONTH_COLUMN]['min']:
            time_map[MONTH_COLUMN]['min'] = month

        day_of_week = result[DAY_OF_WEEK_COLUMN]
        if day_of_week >= time_map[DAY_OF_WEEK_COLUMN]['max']:
            time_map[DAY_OF_WEEK_COLUMN]['max'] = day_of_week
        if day_of_week <= time_map[DAY_OF_WEEK_COLUMN]['min']:
            time_map[DAY_OF_WEEK_COLUMN]['min'] = day_of_week

        day = result[DAY_COLUMN]
        if day >= time_map[DAY_COLUMN]['max']:
            time_map[DAY_COLUMN]['max'] = day
        if day <= time_map[DAY_COLUMN]['min']:
            time_map[DAY_COLUMN]['min'] = day

        period = result[PERIOD_COLUMN]
        if period >= time_map[PERIOD_COLUMN]['max']:
            time_map[PERIOD_COLUMN]['max'] = period
        if period <= time_map[PERIOD_COLUMN]['min']:
            time_map[PERIOD_COLUMN]['min'] = period

    return time_map
        


############################################### ! OLD FLUX #####################################################3

def get_all_baselines(dataset: Dataset, target_column):
    if not dataset.validate_target_column(target_column):
        return {'message': "Target column is not present in the given file"}

    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    # gets the default amount of previous days to use for baseline computation
    # because this number of days is needed for the computation, this amount of days will be skipped
    max_previous_days = get_max_previous_days_for_baseline_computation()
    current_index = max_previous_days * number_of_periods_per_day
    initial_index = current_index

    # todo this might not be multiple request ready, enter method to check other todo
    file_path = get_available_results_path()

    # initializing the result variables
    results_list_for_excel = []
    resultsListForFrontEnd = {}
    infoForFrontEnd = {'periodsList': [], 'daysList': [],
                       'monthsList': [], 'yearsList': []}
    errorMatrix = []
    baselineRequestCount = 0

    # because we skipped the initial_index amount of rows, we are iterating through the whole dataset minus that amount of rows
    for _ in range(dataset.get_number_of_rows() - initial_index):

        # gets the whole row from the dataset with that index
        row = dataset.get_row_from_index(current_index)
        # obtaining needed info from that file
        time_period = int(row['Time Period'])
        day = int(row['Day'])
        month = int(row['Month'])
        year = int(row['Year'])
        day_of_week = int(row['Day of Week'])
        real_target_value = dataset.get_real_target_value(
            time_period, day, month, year, target_column)

        baseline_request_body = get_baseline_server_request_body(
            dataset, current_index, target_column, max_previous_days, time_period, day, month, year)
        if baseline_request_body == False:
            return {'message': "There is no sufficient data on the days before " + str(day) + " " + str(month) + " " + str(year)}

        # sends object to HTTPClient, that will transform to JSON and send to baseline calculation service
        results = request_all_baselines(baseline_request_body)

        if hasattr(results, 'error'):
            return {'message': "error from baseline computation server " + str(results.error)}

        append_error_info(results, real_target_value, errorMatrix)

        # this appends the new info to the object that will be transformed to JSON and returned to the front-end
        # in order to show the results
        # the new resultsListForFrontEnd is the same as before, but with the info from this loop iteration appended
        resultsListForFrontEnd = append_info_for_http_client(
            resultsListForFrontEnd, time_period, day, month, year, day_of_week, results, real_target_value)

        infoForFrontEnd = append_time_info_for_http_client(
            time_period, day, month, year, infoForFrontEnd)

        # same here, but for the excel file, so new formatting and different amount of info is needed
        results_list_for_excel = append_info_for_results_excel(
            results_list_for_excel, time_period, day, month, year, day_of_week, results)

        baselineRequestCount += 1
        # finally, current index is incremented
        current_index += 1

    for i in range(len(errorMatrix)):
        errorMatrix[i][2] = round(errorMatrix[i][2] / baselineRequestCount, 3)

    write_error_matrix_to_excel(errorMatrix, 'errorMatrix.xlsx')

    # writes the header on the excel file with the column names names
    write_excel_header(file_path)
    # takes the results from the computation service and writes them to the excel file
    write_baselines_to_excel2(file_path, results_list_for_excel)
    return {'message': "Success", 'errorMatrix': errorMatrix, "info": infoForFrontEnd, 'data': resultsListForFrontEnd}


def get_all_baselines_weekdays(dataset: Dataset, target_column):
    if not dataset.validate_target_column(target_column):
        return {'message': "Target column is not present in the given file"}

    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    # gets the default amount of previous days to use for baseline computation
    # because this number of days is needed for the computation, this amount of days will be skipped
    # in this case, this is the amount of week days that will be skipped, weekends are ignored
    max_previous_days = get_max_previous_days_for_baseline_computation()

    # this code finds the first index of the file to start calculation, and is skipping weekends
    current_index = 0
    current_previous_weekdays = 0
    while current_previous_weekdays < max_previous_days:
        row = dataset.get_row_from_index(current_index)
        day_of_week = row['Day of Week']
        if day_of_week != 6 and day_of_week != 7:
            current_previous_weekdays += 1
        current_index += number_of_periods_per_day

    initial_index = current_index

    # todo this might not be multiple request ready, enter method to check other todo
    file_path = get_available_results_path()

    results_list_for_excel = []
    resultsListForFrontEnd = {}
    errorMatrix = []
    infoForFrontEnd = {'periodsList': [], 'daysList': [],
                       'monthsList': [], 'yearsList': []}
    baselineRequestCount = 0

    for _ in range(dataset.get_number_of_rows() - initial_index):

        row = dataset.get_row_from_index(current_index)
        day_of_week = int(row['Day of Week'])
        # weekends are skipped
        if day_of_week != 6 and day_of_week != 7:
            # gets the whole row from the dataset with that index
            row = dataset.get_row_from_index(current_index)
            # obtaining needed info from that file
            time_period = int(row['Time Period'])
            day = int(row['Day'])
            month = int(row['Month'])
            year = int(row['Year'])
            real_target_value = dataset.get_real_target_value(
                time_period, day, month, year, target_column)

            baseline_request_body = get_baseline_server_request_body_weekdays(
                dataset, current_index, target_column, max_previous_days, time_period, day, month, year)
            if baseline_request_body == False:
                return {'message': "There is no sufficient data on the days before " + str(day) + " " + str(month) + " " + str(year)}

            # sends object to HTTPClient, that will transform to JSON and send to baseline calculation service
            results = request_all_baselines(baseline_request_body)

            if hasattr(results, 'error'):
                return {'message': "error from baseline computation server " + str(results.error)}

            append_error_info(results, real_target_value, errorMatrix)

            # this appends the new info to the object that will be transformed to JSON and returned to the front-end
            # in order to show the results
            # the new resultsListForFrontEnd is the same as before, but with the info from this loop iteration appended
            resultsListForFrontEnd = append_info_for_http_client(
                resultsListForFrontEnd, time_period, day, month, year, day_of_week, results, real_target_value)

            infoForFrontEnd = append_time_info_for_http_client(
                time_period, day, month, year, infoForFrontEnd)

            # same here, but for the excel file, so new formatting and different amount of info is needed
            results_list_for_excel = append_info_for_results_excel(
                results_list_for_excel, time_period, day, month, year, day_of_week, results)
            baselineRequestCount += 1

        # finally, current index is incremented
        current_index += 1

    for i in range(len(errorMatrix)):
        errorMatrix[i][2] = round(errorMatrix[i][2] / baselineRequestCount, 3)

    # writes the header on the excel file with the column names names
    write_excel_header(file_path)
    # takes the results from the computation service and writes them to the excel file
    write_baselines_to_excel2(file_path, results_list_for_excel)
    return {'message': "Success", 'errorMatrix': errorMatrix, "info": infoForFrontEnd, 'data': resultsListForFrontEnd}


def get_all_baselines_weekends(dataset: Dataset, target_column):

    if not dataset.validate_target_column(target_column):
        return {'message': "Target column is not present in the given file"}

    number_of_periods_per_day = dataset.get_number_of_periods_per_day()
    max_previous_days = get_max_previous_days_for_baseline_computation()
    number_of_rows = dataset.get_number_of_rows()

    current_index = 0
    current_previous_weekends = 0
    while current_previous_weekends < max_previous_days:
        row = dataset.get_row_from_index(current_index)
        day_of_week = row['Day of Week']
        if day_of_week == 6 or day_of_week == 7:
            current_previous_weekends += 1
        current_index += number_of_periods_per_day
        if current_index > number_of_rows - 1:
            return {'message': "There are insuficient weekends to fulfill the request"}

    initial_index = current_index

    # todo this might not be multiple request ready, enter method to check other todo
    file_path = get_available_results_path()

    results_list_for_excel = []
    resultsListForFrontEnd = {}
    infoForFrontEnd = {'periodsList': [], 'daysList': [],
                       'monthsList': [], 'yearsList': []}
    errorMatrix = []
    baselineRequestCount = 0
    for _ in range(dataset.get_number_of_rows() - initial_index):

        row = dataset.get_row_from_index(current_index)
        day_of_week = int(row['Day of Week'])
        # week days are skipped
        if day_of_week == 6 or day_of_week == 7:
            # gets the whole row from the dataset with that index
            row = dataset.get_row_from_index(current_index)
            # obtaining needed info from that file
            time_period = int(row['Time Period'])
            day = int(row['Day'])
            month = int(row['Month'])
            year = int(row['Year'])
            real_target_value = dataset.get_real_target_value(
                time_period, day, month, year, target_column)

            baseline_request_body = get_baseline_server_request_body_weekends(
                dataset, current_index, target_column, max_previous_days, time_period, day, month, year)
            if baseline_request_body == False:
                return {'message': "There is no sufficient data on the days before " + str(day) + " " + str(month) + " " + str(year)}

            # sends object to HTTPClient, that will transform to JSON and send to baseline calculation service
            results = request_all_baselines(baseline_request_body)

            if hasattr(results, 'error'):
                return {'message': "error from baseline computation server " + str(results.error)}

            append_error_info(results, real_target_value, errorMatrix)

            # this appends the new info to the object that will be transformed to JSON and returned to the front-end
            # in order to show the results
            # the new resultsListForFrontEnd is the same as before, but with the info from this loop iteration appended
            resultsListForFrontEnd = append_info_for_http_client(
                resultsListForFrontEnd, time_period, day, month, year, day_of_week, results, real_target_value)

            infoForFrontEnd = append_time_info_for_http_client(
                time_period, day, month, year, infoForFrontEnd)

            # same here, but for the excel file, so new formatting and different amount of info is needed
            results_list_for_excel = append_info_for_results_excel(
                results_list_for_excel, time_period, day, month, year, day_of_week, results)
            baselineRequestCount += 1

        # finally, current index is incremented
        current_index += 1

    for i in range(len(errorMatrix)):
        errorMatrix[i][2] = round(errorMatrix[i][2] / baselineRequestCount, 3)
    # writes the header on the excel file with the column names names
    write_excel_header(file_path)
    # takes the results from the computation service and writes them to the excel file
    write_baselines_to_excel2(file_path, results_list_for_excel)
    return {'message': "Success", 'errorMatrix': errorMatrix, "info": infoForFrontEnd, 'data': resultsListForFrontEnd}


def get_baseline_server_request_body(dataset, current_index, target_column, max_previous_days, time_period, day, month, year):
    # this gets the object with the adjustment values to be used for baseline computation
    # by default, the indexes used are the (target_index - 2) and (target_index - 3)
    # the real value is also stored here, because it is needed for the calculation
    adjustment_values = dataset.get_default_adjustment_values(
        current_index, target_column)
    # this gets the matrix of target values from the given amount of previous_days in the first parameter
    # each row of this matrix has all the values from all the time periods of a certain day
    target_matrix = dataset.get_target_matrix_for_previous_days(
        max_previous_days, time_period, day, month, year, target_column)

    # if the previous method returned false, it means there was not enough data found to fill the matrix, an error is retured to the client
    if target_matrix == False:
        return False

    # building the object that will be transformed to json in order to make the request to the baseline calculation server
    baseline_request_body = {}
    baseline_request_body['consumptionDataset'] = target_matrix
    baseline_request_body['adjustmentValues'] = adjustment_values
    baseline_request_body['targetPeriodIndex'] = int(
        dataset.get_target_time_period_index(time_period))
    return baseline_request_body


def get_baseline_server_request_body_weekdays(dataset, current_index, target_column, max_previous_days, time_period, day, month, year):
    # this gets the object with the adjustment values to be used for baseline computation
    # by default, the indexes used are the (target_index - 2) and (target_index - 3)
    # the real value is also stored here, because it is needed for the calculation
    adjustment_values = dataset.get_default_adjustment_values(
        current_index, target_column)
    # this gets the matrix of target values from the given amount of previous_days in the first parameter
    # each row of this matrix has all the values from all the time periods of a certain day
    # week ends are skipped
    target_matrix = dataset.get_target_matrix_for_previous_weekdays(
        max_previous_days, time_period, day, month, year, target_column)

    # if the previous method returned false, it means there was not enough data found to fill the matrix, an error is retured to the client
    if target_matrix == False:
        return False

    # building the object that will be transformed to json in order to make the request to the baseline calculation server
    baseline_request_body = {}
    baseline_request_body['consumptionDataset'] = target_matrix
    baseline_request_body['adjustmentValues'] = adjustment_values
    baseline_request_body['targetPeriodIndex'] = int(
        dataset.get_target_time_period_index(time_period))
    return baseline_request_body


def get_baseline_server_request_body_weekends(dataset, current_index, target_column, max_previous_days, time_period, day, month, year):
    # this gets the object with the adjustment values to be used for baseline computation
    # by default, the indexes used are the (target_index - 2) and (target_index - 3)
    # the real value is also stored here, because it is needed for the calculation
    adjustment_values = dataset.get_default_adjustment_values(
        current_index, target_column)
    # this gets the matrix of target values from the given amount of previous_days in the first parameter
    # each row of this matrix has all the values from all the time periods of a certain day
    # week days are skipped
    target_matrix = dataset.get_target_matrix_for_previous_weekend_days(
        max_previous_days, time_period, day, month, year, target_column)

    # if the previous method returned false, it means there was not enough data found to fill the matrix, an error is retured to the client
    if target_matrix == False:
        return False

    # building the object that will be transformed to json in order to make the request to the baseline calculation server
    baseline_request_body = {}
    baseline_request_body['consumptionDataset'] = target_matrix
    baseline_request_body['adjustmentValues'] = adjustment_values
    baseline_request_body['targetPeriodIndex'] = int(
        dataset.get_target_time_period_index(time_period))
    return baseline_request_body


def append_error_info(results, real_target_value, errorMatrix):
    resultsList = results.baselines
    # if this is the first time running this function, append base data, each row of the errorMatrix has the previousDays, Method, ErrorSum, Min Error and MaxError
    if len(errorMatrix) == 0:
        for i in range(len(resultsList)):
            errorMatrix.append(
                [resultsList[i].previousDays, resultsList[i].method, 0, 99999, -1])
    for i in range(len(resultsList)):
        result = resultsList[i]
        baseline = result.baseline
        error = round(abs(real_target_value - baseline), 3)
        errorMatrix[i][2] += error
        if error < errorMatrix[i][3]:
            errorMatrix[i][3] = round(float(error), 3)
        if error > errorMatrix[i][4]:
            errorMatrix[i][4] = round(float(error), 3)


def append_time_info_for_http_client(time_period, day, month, year, info):
    if str(time_period) not in info['periodsList']:
        info['periodsList'].append(str(time_period))

    if day not in info['daysList']:
        info['daysList'].append(int(day))

    if month not in info['monthsList']:
        info['monthsList'].append(int(month))

    if year not in info['yearsList']:
        info['yearsList'].append(int(year))

    return info


def append_info_for_http_client(resultsListForFrontEnd, time_period, day, month, year, day_of_week, results, real_target_value):
    if not str(year) in resultsListForFrontEnd:
        resultsListForFrontEnd[str(year)] = {}

    if not str(month) in resultsListForFrontEnd[str(year)]:
        resultsListForFrontEnd[str(year)][str(month)] = {}

    if not str(day) in resultsListForFrontEnd[str(year)][str(month)]:
        resultsListForFrontEnd[str(year)][str(month)][str(day)] = {}

    if not 'Day of week' in resultsListForFrontEnd[str(year)][str(month)][str(day)]:
        resultsListForFrontEnd[str(year)][str(
            month)][str(day)]['Day of week'] = day_of_week

    if not 'Periods' in resultsListForFrontEnd[str(year)][str(month)][str(day)]:
        resultsListForFrontEnd[str(year)][str(month)][str(day)]['Periods'] = []

    periodInfo = {}
    periodInfo['Time Period'] = str(time_period)
    periodInfo['Baselines'] = results
    resultsList = results.baselines

    periodInfo['Real Value'] = int(real_target_value)
    resultsListForFrontEnd[str(year)][str(month)][str(
        day)]['Periods'].append(periodInfo)
    return resultsListForFrontEnd


def append_info_for_results_excel(results_list_for_excel, time_period, day, month, year, day_of_week, results):
    results_and_info = {}
    results_and_info['results'] = results
    results_and_info['time_period'] = time_period
    results_and_info['day'] = day
    results_and_info['month'] = month
    results_and_info['year'] = year
    results_and_info['day_of_week'] = day_of_week
    results_list_for_excel.append(results_and_info)
    return results_list_for_excel
