import json

SETTINGS_FILE_PATH = 'settings.json'

class SettingsManager():

    def __init__(self):
        self.settings_file_path = 'settings.json'

    def get_max_baseline_window():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['max_baseline_window']
        
    def get_max_previous_days_for_baseline_computation():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['max_previous_days_for_baseline_computation']

    def get_time_period_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['time_period_column_title']

    def get_day_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['day_column_title']

    def get_day_of_week_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['day_of_week_column_title']

    def get_month_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['month_column_title']

    def get_year_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['year_column_title']

    def get_is_filtered_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['is_filtered_column_title']

    def get_is_filtered():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['is_filtered']

    def get_is_not_filtered():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['is_not_filtered']

    def get_previous_indexes_for_adjustment():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['previous_indexes_for_adjustment']