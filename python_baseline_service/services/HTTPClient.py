import requests
from pprint import pprint
import json
from collections import namedtuple

class HTTPClient:

    def get_available_methods():
        response = requests.get('http://localhost:4200/availableMethods')
        json_response = response.json()
        object_response = json.loads(json.dumps(json_response), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        return object_response

    def request_baselines_from_baseline_calculation_service(data):
        response = requests.get('http://localhost:4200/selectedBaselines', json=data)
        json_response = response.json()
        object_response = json.loads(json.dumps(json_response), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        return object_response


def request_all_baselines(data):
    response = requests.get('http://localhost:4200/allBaselines', json=data)
    json_response = response.json()
    #pprint(json_response)
    # this converts json to python object
    object_response = json.loads(json.dumps(json_response), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    return object_response

def request_baseline_by_multiplication_of_average_values(data):
    # todo if debugging or if deployed
    response = requests.get('http://localhost:4200/getBaselineByMultiplicationOfAverageValues', json=data)
    json_response = response.json()
    #pprint(json_response)
    # this converts json to python object
    object_response = json.loads(json.dumps(json_response), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    return object_response


def request_baseline_by_subtraction_of_average_values(data):
    # todo if debugging or if deployed
    response = requests.get('http://localhost:4200/getBaselineBySubtractingAverageValues', json=data)
    json_response = response.json()
    #pprint(json_response)
    # this converts json to python object
    object_response = json.loads(json.dumps(json_response),
                                 object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    return object_response


def request_multiple_baselines(data):
    # todo if debugging or if deployed
    response = requests.get('http://localhost:4200/getMultipleBaselines', json=data)
    json_response = response.json()
    #pprint(json_response)
    # this converts json to python object
    object_response = json.loads(json.dumps(json_response),
                                 object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    return object_response