from ..model.Dataset import Dataset
from ..services.CorrelationCalculator import get_correlation_between_target_value_and_previous_target_values
from ..services.CorrelationCalculator import get_dataframe_of_target_value_and_previous_target_values
from .SettingsManager import SettingsManager
import pandas as pd


class CorrelationService:

    def get_all_correlations(dataset: Dataset):
        correlations = dataset.data.corr(method='pearson')
        correlations = correlations.round(decimals=3)
        columns = correlations.columns.values.tolist()
        correlations = correlations.to_numpy().tolist()
        correlations.insert(0, columns)
        return correlations


#gets all correlations, from previous day consumption values and with other column values from the dataset
def get_all_correlations1(dataset: Dataset, target_column):
    if not dataset.validate_target_column(target_column):
        return {'message': "Target column is not present in the given file"}
    
    previous_days_dataframe = get_dataframe_of_target_value_and_previous_target_values(dataset, target_column)
    #previous_days_matrix = previous_days_dataframe.to_numpy().tolist()
    
    #this gets the correlation between all columns of the dataset
    data = dataset.data
    max_previous_days = SettingsManager.get_max_previous_days_for_baseline_computation()

    #this removes the first X days, that are used for history purposes only
    data.drop(data.head(max_previous_days * dataset.get_number_of_periods_per_day()).index, inplace=True)
    data = data.reset_index(drop=True)

    #merging the two dataframes
    data = pd.concat([data, previous_days_dataframe], axis=1)

    #rearranging column order to make it so that the target column comes first
    cols = data.columns.tolist()
    cols.remove(target_column)
    cols.insert(0, target_column)
    data = data[cols]

    overall_results = data.corr(method='pearson')
    #instead of returning null values, returning the number 99 because a correlation can never have 99 as value
    overall_results = overall_results.fillna(99)
    overall_results = overall_results.round(decimals=3)

    
    overall_results.to_excel("cors.xlsx")
    columns = overall_results.columns.values.tolist()

    overall_results = overall_results.to_numpy().tolist()
    overall_results.insert(0, columns)

    return {"message": "Success", 'correlations': overall_results}


#? old one, unused
#gets all correlations, from previous day consumption values and with other column values from the dataset
def get_all_correlations2(dataset: Dataset, target_column):

    if not dataset.validate_target_column(target_column):
        return {'message': "Target column is not present in the given file"}

    #this gets the dataframe with correlation values from the X previous_days defined in the configuration file
    correlations_with_previous_days = get_correlation_between_target_value_and_previous_target_values(dataset, target_column)
    #instead of returning null values, returning the number 99 because a correlation can never have 99 as value
    correlations_with_previous_days = correlations_with_previous_days.fillna(99)
    correlations_with_previous_days = correlations_with_previous_days.round(decimals=3)
    #to return to the client, transform to a matrix
    correlations_with_previous_days_list = correlations_with_previous_days.to_numpy().tolist()
    #add the columns to the matrix, at the 0 index
    columns = correlations_with_previous_days.columns.values.tolist()
    correlations_with_previous_days_list.insert(0, columns)


    #this gets the correlation between all columns of the dataset
    data = dataset.data
    max_previous_days = get_max_previous_days_for_baseline_computation()
    data = data.iloc[max_previous_days:]
    overall_results = data.corr(method='pearson')
    #instead of returning null values, returning the number 99 because a correlation can never have 99 as value
    overall_results = overall_results.fillna(99)
    overall_results = overall_results.round(decimals=3)
    #to return to the client, transform to a matrix
    overall_results_list = overall_results.to_numpy().tolist()
    #add the columns to the matrix, at the 0 index
    columns = overall_results.columns.values.tolist()
    overall_results_list.insert(0, columns)

    return {"message": "Success", 'correlations_with_previous_days': correlations_with_previous_days_list, 'overall_results': overall_results_list}
