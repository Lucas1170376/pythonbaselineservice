from .SettingsManager import SettingsManager
from .HTTPClient import *
from ..model.Dataset import Dataset
from .ExcelIO import write_baselines_to_excel
from .ExcelIO import write_baselines_to_excel2
from .ExcelIO import write_excel_header
from .ExcelIO import write_error_matrix_to_excel
from ..services import ExcelIO
from .FileManager import get_available_results_path
from .FileManager import get_dataset_info_for_result_presentation
from ..factory import DatasetFactory
from ..services.FileManager import FileManager
from django.conf import settings

import pandas as pd

YEAR_COLUMN = SettingsManager.get_year_column_title()
MONTH_COLUMN = SettingsManager.get_month_column_title()
DAY_OF_WEEK_COLUMN = SettingsManager.get_day_of_week_column_title()
DAY_COLUMN = SettingsManager.get_day_column_title()
PERIOD_COLUMN = SettingsManager.get_time_period_column_title()
IS_FILTERED_COLUMN_TITLE = SettingsManager.get_is_filtered_column_title()
IS_FILTERED = SettingsManager.get_is_filtered()
IS_NOT_FILTERED = SettingsManager.get_is_not_filtered()
PREVIOUS_INDEXES_FOR_ADJUSTMENT = SettingsManager.get_previous_indexes_for_adjustment()

class BaselineService:

    def get_baselines(dataset: Dataset, target_column, target_days_of_week, filters, methods_list):
        if not dataset.validate_target_column(target_column):
            return {'message': 'The chosen target column is not in the file'}

        week_day_filtered_data = BaselineService.get_day_of_week_filtered_dataset(dataset.data, target_days_of_week)
        new_dataset = DatasetFactory.create_dataset_from_dataframe(week_day_filtered_data)
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()

        if not BaselineService.validate_historical_data_size(new_dataset, number_of_periods_per_day):
            return {'message': 'There is not sufficient historical data to compute baselines'}

        initial_index = SettingsManager.get_max_baseline_window() * dataset.get_number_of_periods_per_day()
        current_index = initial_index
        results_list = []

        for _ in range(new_dataset.get_number_of_rows() - initial_index):
            results = BaselineService.compute_all_baselines(new_dataset, current_index, target_column, filters, methods_list)
            if results == False:
                current_index += 1
                continue
            if hasattr(results, 'message'):
                return results
            results_list.append(results)
            current_index += 1

        if len(results_list) == 0:
            return {'message': 'There is not sufficient historical data to compute baselines with your parameters'}

        results_info = BaselineService.get_results_info(results_list)
        error_info = BaselineService.get_error_info(results_list)
        time_info = BaselineService.get_time_information(results_list)
        results_path = settings.MEDIA_URL + FileManager.get_timed_results_filename()
        ExcelIO.write_baselines_to_excel(results_path, results_list)
        return {'results': results_list, 'resultsInfo': results_info, 'errors': error_info, 'timeInfo': time_info, 'resultsPath': results_path}


    def compute_all_baselines(dataset: Dataset, index, target_column, filters, methods_list):
        row = dataset.get_row_from_index(index)
        year = int(row[YEAR_COLUMN])
        month = int(row[MONTH_COLUMN])
        day_of_week = int(row[DAY_OF_WEEK_COLUMN])
        day = int(row[DAY_COLUMN])
        time_period = int(row[PERIOD_COLUMN])
        #obtaining the real target value to compute the error later
        real_target_value = float(dataset.get_real_target_value(time_period, day, month, year, target_column))
        #prepares the request body for the baseline calculation service, if there was no data to build it, skip this iteration of the loop
        baseline_request_body = BaselineService.get_baseline_calculation_service_request_body(dataset, index, target_column, time_period, day, month, year, filters, row, methods_list)
        if baseline_request_body == False:
            return False
        results = HTTPClient.request_baselines_from_baseline_calculation_service(baseline_request_body)
        if hasattr(results, 'error'):
            return {'message': "error from baseline computation server " + str(results.error)}
        return {YEAR_COLUMN: year, MONTH_COLUMN: month, DAY_OF_WEEK_COLUMN: day_of_week, DAY_COLUMN: day, 
                                    PERIOD_COLUMN: time_period, 'Results': results, 'Real Value': real_target_value }


    def get_baseline_calculation_service_request_body(dataset, index, target_column, time_period, day, month, year, filters, target_row, methods_list):
        historical_data = dataset.get_historical_data(time_period, day, month, year, target_column, filters, target_row)
        if historical_data == False:
            return False
        
        adjustment_values = dataset.get_adjustment_values(index, target_column)
        if adjustment_values == False:
            return False

        baseline_request_body = {}
        baseline_request_body['consumptionDataset'] = historical_data
        baseline_request_body['adjustmentValues'] = adjustment_values
        baseline_request_body['targetPeriodIndex'] = len(PREVIOUS_INDEXES_FOR_ADJUSTMENT)
        baseline_request_body['methodsList'] = methods_list
        return baseline_request_body


    def get_day_of_week_filtered_dataset(data, days_of_week_list):
        return data[(data[DAY_OF_WEEK_COLUMN].isin(days_of_week_list))]

    def validate_historical_data_size(dataset: Dataset, number_of_periods_per_day):
        number_of_rows = dataset.get_number_of_rows()
        max_baseline_window = SettingsManager.get_max_baseline_window()
        return number_of_rows > max_baseline_window * number_of_periods_per_day

    def get_results_info(results_list):
        print(results_list)
        info_map = {}
        info_map['previousDays'] = []
        info_map['methods'] = []
        for result in results_list:
            results = result['Results']
            baselines = results[0]
            for baseline in baselines:
                previous_days_used = baseline[0]
                method = baseline[1]
                if previous_days_used not in info_map['previousDays']:
                    info_map['previousDays'].append(previous_days_used)
                if method not in info_map['methods']:
                    info_map['methods'].append(method)
        return info_map


    def get_error_info(results_list):
        errors_map = {}
        for result in results_list:
            results = result['Results']
            real_value = result['Real Value']
            baselines = results[0]
            for baseline in baselines:
                previous_days_used = baseline[0]
                method = baseline[1]
                baseline_value = round(float(baseline[2]),3)
                # gets the error rounded to 3 decimal cases
                error = round(float(abs(real_value - baseline_value)), 3)
                #appends the information to the map, in order to return it to the client
                errors_map = BaselineService.append_error_information(errors_map, previous_days_used, method, error)
        return errors_map

    def append_error_information(errors_map, previous_days, method, error):
        map_key = str(previous_days)+";"+str(method)
        if map_key not in errors_map:
            errors_map[map_key] = {'minError': 999999.000, 'maxError': 0.000, 'errorSum': 0.000, 'errorCount': 0}
        if error <= errors_map[map_key]['minError']:
            errors_map[map_key]['minError'] = error
        if error >= errors_map[map_key]['maxError']:
            errors_map[map_key]['maxError'] = error
        errors_map[map_key]['errorSum'] += error
        errors_map[map_key]['errorCount'] += 1
        return errors_map

    def get_time_information(results_list):
        time_map = {YEAR_COLUMN: {'min': 999999, 'max': 0},MONTH_COLUMN: {'min': 999999, 'max': 0},DAY_OF_WEEK_COLUMN: {'min': 999999, 'max': 0},
        DAY_COLUMN: {'min': 999999, 'max': 0},PERIOD_COLUMN: {'min': 999999, 'max': 0}}
        for result in results_list:
            year = result[YEAR_COLUMN]
            if year >= time_map[YEAR_COLUMN]['max']:
                time_map[YEAR_COLUMN]['max'] = year
            if year <= time_map[YEAR_COLUMN]['min']:
                time_map[YEAR_COLUMN]['min'] = year

            month = result[MONTH_COLUMN]
            if month >= time_map[MONTH_COLUMN]['max']:
                time_map[MONTH_COLUMN]['max'] = month
            if month <= time_map[MONTH_COLUMN]['min']:
                time_map[MONTH_COLUMN]['min'] = month

            day_of_week = result[DAY_OF_WEEK_COLUMN]
            if day_of_week >= time_map[DAY_OF_WEEK_COLUMN]['max']:
                time_map[DAY_OF_WEEK_COLUMN]['max'] = day_of_week
            if day_of_week <= time_map[DAY_OF_WEEK_COLUMN]['min']:
                time_map[DAY_OF_WEEK_COLUMN]['min'] = day_of_week

            day = result[DAY_COLUMN]
            if day >= time_map[DAY_COLUMN]['max']:
                time_map[DAY_COLUMN]['max'] = day
            if day <= time_map[DAY_COLUMN]['min']:
                time_map[DAY_COLUMN]['min'] = day

            period = result[PERIOD_COLUMN]
            if period >= time_map[PERIOD_COLUMN]['max']:
                time_map[PERIOD_COLUMN]['max'] = period
            if period <= time_map[PERIOD_COLUMN]['min']:
                time_map[PERIOD_COLUMN]['min'] = period
        return time_map