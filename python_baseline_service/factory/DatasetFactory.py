from ..services.ExcelIO import read_excel_to_dataframe
from ..model.Dataset import Dataset

def create_dataset_from_dataframe(dataframe):
    return Dataset(dataframe)
